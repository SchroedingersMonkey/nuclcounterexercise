//GC content berechnen
//Dinukleotidfrequenzen berechnen

//zusatz: gesamt-Ausgabedatei mit GC content und 
//Dinukletoidfrequenzen

nextflow.enable.dsl=2

process split_file{
  
  input:
    path infile
  output:
    path "${infile}.line*"
  script:
    """
    split -l 2 ${infile} -d ${infile}.line
    """
    
}

process gcContent{
    publishDir "/tmp/gcContent/", mode: "copy", overwrite: true

    input:
        tuple path(infile),path(python)
        //path infile
        //path python
     
    output:
        path "${infile}.gcCount"
    script:
    """
    python3 ${python} ${infile} > ${infile}.gcCount
    """

}


workflow {

inchannel = channel.fromPath("./sequences.fasta")
inPython1 = channel.fromPath("./gcContent.py")

splitfiles = split_file(inchannel).flatten()

kombiniert = splitfiles.combine(inPython1)
gccounter = gcContent(kombiniert)

}