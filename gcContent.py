#GC-Content zähler

import sys

def gcContent(infile, outfile="/tmp/pyTest.txt"):
    
    gcCounter = []
    seqCount = -1

    with open(infile) as datei:
        for line in datei:
            if line[0] != ">":
                gcCounter.append(0)
                seqCount += 1
                for i in line:
                    if i == "G" or i == "C":
                        gcCounter[seqCount] +=1 

    #print(gcCounter)

    """
    with open(outfile,"w") as datei:
        for i in range(len(gcCounter)):
            datei.write(str(gcCounter[i]))
            datei.write("\n")
    """
    return gcCounter

ergebnis = gcContent(sys.argv[1])#,sys.argv[2])

for i in range(len(ergebnis)):
    print(ergebnis[i])
